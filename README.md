# README
This is the personal website for [Adam Brenner](https://aeb.io)

# Build Information
This site is built using one tool: hugo.

## Hugo
Hugo is what makes this website possible. It converts markdown and my template
into static html pages. Preview the site by running 

    $ hugo server --disableFastRender
