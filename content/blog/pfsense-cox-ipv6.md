---
title: "pfSense Cox IPv6"
date: 2018-04-08T09:15:55-07:00
description: ""
tags: []
categories: []
draft: true
---

#todo
Under WAN type the following:
enable request IPv6 prefix via IPv6 link
DHCP v6 prefix size: /56
Send IPv6 prefix hint: check
Hit save

Then for each LAN, make sure the following is set:

IPv6 Configuration Type: Track Interface
Track IPv6 Interface
IPv6 Interface: WAN
IPv6 Prefix ID: choose a unique value for each LAN interface between 0 and ff

*Might* have to reboot modem and pfsense

Lastly, go ahead and allow all ICMP traffic for v6 on your WAN

Test your settings by visiting: ipv6-test.com

Set your DNS servers under general setup
