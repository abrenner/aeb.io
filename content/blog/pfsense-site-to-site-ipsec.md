---
title: "Pfsense Site to Site Ipsec"
date: 2018-04-07T12:04:38-07:00
description: ""
tags: []
categories: []
draft: true
---

# notes
on SLC:
Create IPSec Tunnel Phase 1

General Information
Key Exchange: IKEv2
Internet Protocl: IPv4
Interface: VPN-Alias
Remote Gateway: SNA Address

Phase 1 Prospal
Authencation Method: Mutal PSK
My identifer: My IP Address
My identifer: Peer IP Address
Pre-share Key: 

Phase 1 Propsal: Encryption
AES, 256 bits, SHA256 Hash, 16(4096) DH Group

Do the same for SNA but swap the remote gateway address to SLC


Phase 2 on SLC:

General Information
Mode: Tunnel IPv4
Local Network: LAN Subnet
NAT/BINAT: None
Remote Network: Network: SNA's address (10.12.71.0/24)

Phase 2 Propsal
Protocl: ESP
Encryption Algo: AES256-GCM AUTO
Hash Algo: SHA256
PFS Key Group: 16 (4096)
Ping host: SLC's address (10.10.10.1)

Do the same for SNA but swap the remote network and ping host


Do not forget to add firewall rules so the two hosts can talk to each other
This will be done on the WAN interface
Then the IPsec interface needs to add allow rules as well
