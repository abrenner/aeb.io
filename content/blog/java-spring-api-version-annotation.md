---
title: "Java Spring Api Version Annotation"
date: 2018-03-25T11:45:58-07:00
description: ""
tags: [
    "java",
    "java spring"
]
categories: [
    "programming",
    "how-to"
]
draft: true
---

# TODO
Talk about how I got the @ApiVersion to work with a Java Spring Application
Incude source code
Maybe talk about the different methods of getting different API versions (URL, header, param)
Source for all my work:
https://stackoverflow.com/questions/20198275/how-to-manage-rest-api-versioning-with-spring
https://stackoverflow.com/questions/36744678/spring-boot-swagger-2-ui-custom-requestmappinghandlermapping-mapping-issue/45340695#45340695
