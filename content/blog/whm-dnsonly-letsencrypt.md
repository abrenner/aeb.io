---
title: "WHM DNSOnly LetsEncrypt"
date: 2019-03-07T00:00:00-07:00
description: ""
tags: [
    "cpanel",
    "whm",
    "ssl"
]
categories: [
    "how-to",
    "hosting"
]
draft: true
---

# Notes
install certbot on WHM DNSOnly. 
yum install epel-release
 see https://fedoraproject.org/wiki/EPEL#How_can_I_use_these_extra_packages.3F
yum install certbot
certbot certonly -m abuse@solidnetwork.org --agree-tos --standalone


output
[root@dns ~]# certbot certonly --standalone -m abuse@solidnetwork.org --agree-tos
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
Starting new HTTPS connection (1): acme-v02.api.letsencrypt.org

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about our work
encrypting the web, EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: no
Please enter in your domain name(s) (comma and/or space separated)  (Enter 'c'
to cancel): dns.netops.me www.dns.netops.me
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for dns.netops.me
http-01 challenge for www.dns.netops.me
Waiting for verification...
Cleaning up challenges
Resetting dropped connection: acme-v02.api.letsencrypt.org

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/dns.netops.me/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/dns.netops.me/privkey.pem
   Your cert will expire on 2019-06-06. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

[root@dns ~]#
