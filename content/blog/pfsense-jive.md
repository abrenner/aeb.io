---
title: "pfSense Jive"
date: 2018-03-22T22:22:04-07:00
description: ""
tags: [
    "pfsense",
    "voip"
]
categories: [
    "networking",
    "how-to"
]
draft: true
---

# notes
some passnic phones use ports 5080 and 5081 where polycoms use 5060 and 5061
If you experince one-way or no audio but the phones register, it is most likely RTP issues. SIP Is used for the configuration and all "metadata" about a phone call.. but the job of audio is left to RTP. Each provider has different sets of RTP ports but for this instance with Jive the RTP ports were 20,000 - 60,000. These RTP ports NEED to be static mapping. This is contradictory to what is written on their support page (perhaps now it has been updated based on my feedback).

# How to setup pfSense with Jive (VoIP Provider)
This how to guide is what I used when setting up pfSense to work with [Jive](https://jive.com/). Jive offically does not endorse customers running pfSense and from the few level 3 support staff I have interacted with they are unfimilar with pfSense, dispite it being a very popular firewall and router software.

# About Jive
[Jive](https://jive.com/) is a remote host PBX solution where clients (IP phones) connect to Jive's network via the SIP portocl on ports 5060 and 5061. A level 3 support staff also mentioned the use of ports 5080 and 5081 but it is unclear what those are used for as its not publically listed on any of their documenation.

# Important Settings
There are a few settings that need to be done on pfSense in order to get things working. 

    * Increase UDP states
    * Ensure NAT port mappings are ***randomzied***
    * Allow traffic to Jive's network
    * Use a well trusted DNS provider

## Increase UDP States
All VoIP devices (soft or hard phones) use [SIP](https://en.wikipedia.org/wiki/Session_Initiation_Protocol) portocl which in term uses UDP. The reason for UDP vs let's say TCP is the overhead that TCP offers. The reason this is important is because humans will notice audio delay after about 100ms aka latency. For phone calls this is when you stop speaking and wait for the person on the remote end to hear you.

In the context for pfSense 

# Troubleshooting Steps
## Phones not registered
