---
categories: ["projects"]
date: "2018-03-01T10:00:00-07:00"
tags: ["projects"]
title: "Projects"
projects: true
draft: true
---
Adam Brenner academic, professional and personal projects. This includes open source projects.
